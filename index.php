<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");

echo "Name : ".$sheep->name. "<br>"; // "shaun"
echo "Legs : ".$sheep->legs. "<br>"; // 4
echo "Cold blooded : ".$sheep->cold_blooded. "<br>"; // "no"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
// "Auooo"
echo "Name : ".$sungokong->name."<br>";
echo "Legs : ".$sungokong->legs."<br>";
echo "cold blooded : ".$sungokong->cold_blooded."<br>";
echo "Jump : ".$sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

echo "Name : ".$kodok->name."<br>";
echo "Legs : ".$kodok->legs."<br>";
echo "cold blooded : ".$kodok->cold_blooded."<br>";
echo "Jump : ".$kodok->jump();
?>